package controller;

import bo.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet( name = "personController", urlPatterns = {"/persons"} )
public class PersonController extends HttpServlet {
	
	private static final String PAGE_LIST_JSP = "/WEB-INF/jsp/persons_list.jsp";
	private static final String PAGE_EDIT_JSP = "/WEB-INF/jsp/person_details.jsp";
	
	@Override
	protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
		HttpSession session = request.getSession( true );
		List<Person> dataSession = ( List<Person> ) session.getAttribute( "persons" );
		
		if ( null == dataSession ) {
			dataSession = new ArrayList<>();
			dataSession.add( new Person( 0, "Séga", "S", 500 ) );
			dataSession.add( new Person( 1, "Lass", "S", 1000 ) );
			session.setAttribute( "persons", dataSession );
		}
		
		String action = request.getParameter( "action" );
		if ( null == action ) {
			request.getRequestDispatcher( PAGE_LIST_JSP ).forward( request, response );
		} else {
			int id;
			try {
				id = Integer.parseInt( request.getParameter( "id" ) );
			} catch ( Exception e ) {
				id = -1;
			}
			switch ( action ) {
				case "details":
					Person currentPerson;
					if ( id >= 0 && id < dataSession.size() ) {
						currentPerson = dataSession.get( id );
					} else {
						currentPerson = new Person();
					}
					request.setAttribute( "currentPerson", currentPerson );
					request.getRequestDispatcher( PAGE_EDIT_JSP ).forward( request, response );
					break;
				case "delete":
					if ( id >= 0 && id < dataSession.size() ) {
						dataSession.remove( id );
					}
				default:
					response.sendRedirect( request.getContextPath() + "/persons" );
			}
		}
	}
	
	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
		
		HttpSession session = request.getSession( true );
		List<Person> dataSession = ( List<Person> ) session.getAttribute( "persons" );
		
		String idStr = request.getParameter( "form-id" );
		String name = request.getParameter( "form-name" );
		String email = request.getParameter( "form-email" );
		double salary;
		try {
			salary = Double.parseDouble( request.getParameter( "form-salary" ) );
		} catch ( Exception e ) {
			salary = 0;
		}
		if ( idStr == null ) idStr = "-1";
		int id = Integer.parseInt( idStr );
		Person currentPerson;
		if ( id >= 0 && id < dataSession.size() ) {
			currentPerson = dataSession.get( id );
			currentPerson.setName( name );
			currentPerson.setEmail( email );
			currentPerson.setSalary( salary );
		} else {
			currentPerson = new Person();
			currentPerson.setName( name );
			currentPerson.setEmail( email );
			currentPerson.setSalary( salary );
			dataSession.add( currentPerson );
		}
		
		response.sendRedirect( request.getContextPath() + "/persons" );
	}
}
